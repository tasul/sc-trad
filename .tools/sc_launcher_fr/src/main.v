module main

import os
import net.http
import json
import dialog

struct Language {
pub:
	name string
	url string
	path string
}

const (
	default_star_citizen_executable_path  = 'C:\\Program Files\\Roberts Space Industries\\StarCitizen\\LIVE\\Bin64\\StarCitizen.exe'
	default_rsi_launcher_executable_path  = 'C:\\Program Files\\Roberts Space Industries\\RSI Launcher\\RSI Launcher.exe'
	default_save_file_location = '${os.home_dir()}\\.tasul\\config_launcher.conf'
	default_user_path = 'C:\\Program Files\\Roberts Space Industries\\StarCitizen\\LIVE\\user.cfg'
	default_lang = 'french_(france)'
	default_lang_url = 'https://trad.sc.tasul.fr/api/file/fr'
	default_save_file = SaveFile {
		sc_path: default_star_citizen_executable_path
		rsi_path: default_rsi_launcher_executable_path
		lang: default_lang
	}
)

struct SaveFile {
pub:
	sc_path string [json: 'sc-path']
	rsi_path string [json: 'rsi-path']
	lang string
pub mut:
	url ?string [json: 'override-lang-url']
}

fn main() {
	// Load saved file
	savefile := load_saved_file()

	// Find star citizen executable
	star_citizen_executable_path := find_sc(savefile.sc_path, default_star_citizen_executable_path)

	// Find translation file : star_citizen_executable_path but split until C:\\Program Files\\Roberts Space Industries\\StarCitizen\\LIVE\\
	// This work with PTU Star Citizen too.
	splitted_star_citizen_path := star_citizen_executable_path.split('\\')

	launcher_path := find_rsi(savefile.rsi_path, splitted_star_citizen_path[0..splitted_star_citizen_path.len-4].join('\\') + '\\RSI Launcher\\RSI Launcher.exe')
	translation_path := splitted_star_citizen_path[0..splitted_star_citizen_path.len-2].join('\\') + '\\data\\Localization\\${savefile.lang}\\global.ini'
	user_path := splitted_star_citizen_path[0..splitted_star_citizen_path.len-2].join('\\') + '\\user.cfg'

	// read user.cfg add g_language only if it's not already present
	mut user_cfg := os.read_file(user_path) or { '' }

	mut find := false
	// for each lines, if g_language is present, set find to true
	for line in user_cfg.split('\n') {
		if line.contains('g_language') {
			find = true
			break
		}
	}

	if !find {
		// add g_language to user.cfg
		user_cfg += '\ng_language = ${os.base(os.dir(translation_path))}\n'
		unsafe { os.write_file(user_path, user_cfg) }
	}

	if !os.exists(default_save_file_location) {
		translation_dir := os.dir(default_save_file_location)
		os.mkdir_all(translation_dir, mode: 0o777) or {
			dialog.message('Impossible de créer le dossier de sauvegarde : ${translation_dir}')
		}
		mut newsavefile := SaveFile {
			sc_path: star_citizen_executable_path
			rsi_path: launcher_path
			lang: savefile.lang
		}
		if savefile.url != none {
			newsavefile.url = savefile.url
		}
		save_to_file(newsavefile)
	}

	translation_dir := os.dir(translation_path)
	if !os.exists(translation_dir) {
		os.mkdir_all(translation_dir, mode: 0o777) or {
			dialog.message('Impossible de créer le dossier de traduction : ${translation_dir}')
			exit(1)
		}
	}
	lang_url := savefile.url or { default_lang_url }
	download_translation(lang_url, translation_path)

	// search for config

	// Run star citizen executable
	os.execute_or_panic(os.quoted_path(launcher_path))
}

fn load_saved_file() SaveFile {
	mut save_file_string := ''
	// Read json from user home directory
	if os.exists(default_save_file_location) {
		// Read json from file
		save_file_string = os.read_file(default_save_file_location) or { '' }
	}
	return json.decode(SaveFile, save_file_string) or { default_save_file }
}

fn save_to_file(savefile SaveFile) {
	unsafe { os.write_file(default_save_file_location, json.encode(savefile)) }
}

fn find_sc(input_path string, default_path string) string {
	path := path_or_default(input_path, default_path)
	if os.exists(path) {
		return path
	}
	dialog.message('Star Citizen n\'a pas été touvé automatiquement,\n\nVeuillez sélectionner le fichier starcitizen.exe dans le dossier d\'installation de Star Citizen.\nHabituellement :\n${default_star_citizen_executable_path}')

	selected_file := find_executable(os.dir(default_star_citizen_executable_path), os.base(default_star_citizen_executable_path))

	return selected_file
}

fn find_rsi(input_path string, default_path string) string {
	path := path_or_default(input_path, default_path)
	if os.exists(path) {
		return path
	}
	dialog.message('RSI Launcher n\'a pas été touvé automatiquement,\n\nVeuillez sélectionner le fichier "RSI Launcher.exe" dans le dossier d\'installation de RSI Launcher.\nHabituellement :\n${default_rsi_launcher_executable_path}')

	selected_file := find_executable(os.dir(default_rsi_launcher_executable_path), os.base(default_rsi_launcher_executable_path))

	return selected_file
}

fn path_or_default(path string, default_path string) string {
	if os.exists(path) {
		return path
	}
	return default_path
}

fn find_executable(path string, executable string) string {
	selected_file := dialog.file_dialog(dialog.FileDialogOptions{
		action: .open,
		path: path,
		filename: executable,
	}) or { '' }

	if selected_file == '' {
		dialog.message('Aucun fichier sélectionné.\n\nAbandon.')
		exit(1)
	}

	if !('${executable:s}' in '${selected_file:s}'.split('\\')) {
		dialog.message('Vous n\'avez pas sélectionné "${executable}" mais "${os.base(selected_file)}".\n\nAbandon.')
		exit(1)
	}

	return selected_file
}

fn download_translation(lang string, path string) {
	http.download_file(lang, path) or {
		dialog.message('Impossible de télécharger la traduction : ${lang} (${path})')
		exit(1)
	}
}
