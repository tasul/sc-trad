# Launcher

## How to use ?

Just run the `launcher.exe` file. You can place it anywhere you want in your computer.

It will configure for you the user.cfg and download the latest version of the translation.

## How to customize settings.

If you already executed the launcher once, it will create a directory in your home directory with a json file inside, you can edit the json file and customize any value you want. The next start will use your custom settings.

## How to build it ?

You have to install the `V` compiler. And then run `v -prod -o dist/launcher.exe -os windows .` when you are in the .tools/sc_launcher_fr directory of this repository.
