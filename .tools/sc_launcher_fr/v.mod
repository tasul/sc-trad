Module {
	name: 'sc_launcher_fr'
	description: 'Always update your french translation when launching your Star Citizen session.'
	version: '1.0.0-beta.1'
	license: 'MIT'
	dependencies: []
}
